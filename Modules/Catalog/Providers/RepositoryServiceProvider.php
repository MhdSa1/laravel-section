<?php

namespace Modules\Catalog\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Catalog\Interfaces\V1\CategoryRepositoryInterface;
use Modules\Catalog\Interfaces\V1\ItemRepositoryInterface;
use Modules\Catalog\Interfaces\V1\MenuRepositoryInterface;
use Modules\Catalog\Repositories\V1\CategoryRepository;
use Modules\Catalog\Repositories\V1\ItemRepository;
use Modules\Catalog\Repositories\V1\MenuRepository;

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->app->bind(ItemRepositoryInterface::class, ItemRepository::class);
        $this->app->bind(MenuRepositoryInterface::class, MenuRepository::class);

        
    }
}
