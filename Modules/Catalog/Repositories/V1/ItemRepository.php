<?php

namespace Modules\Catalog\Repositories\V1;

use Modules\Catalog\Actions\ItemStoreAction;
use Modules\Catalog\Entities\Item;
use Modules\Catalog\Http\Resources\Items\ItemResource;
use Modules\Catalog\Interfaces\V1\ItemRepositoryInterface;

class ItemRepository implements ItemRepositoryInterface
{
    public function index($request)
    {
        return ItemResource::collection(Item::with(['category'])->get());
    }

    public function store($request)
    {
        $item = ItemStoreAction::execute($request);
        return new ItemResource($item);
    }

    public function show($request, $item)
    {
        $item->load(['category']);
        return new ItemResource($item);
    }
}
