<?php

namespace Modules\Catalog\Repositories\V1;

use Modules\Catalog\Actions\CategoryStoreAction;
use Modules\Catalog\Entities\Category;
use Modules\Catalog\Http\Resources\Categories\CategoryResource;
use Modules\Catalog\Interfaces\V1\CategoryRepositoryInterface;

class CategoryRepository  implements CategoryRepositoryInterface
{
    public function index($request)
    {
        return   CategoryResource::collection(Category::with(['parent', 'children'])->get());
    }

    public function store($request)
    {
        $category = CategoryStoreAction::execute($request);

        return new CategoryResource($category);
    }

    public function show($request, $category)
    {
        $category->load(['parent', 'children']);
        return   new CategoryResource($category);
    }
}
