<?php

namespace Modules\Catalog\Repositories\V1;

use Modules\Catalog\Entities\Menu;
use Modules\Catalog\Http\Resources\Menus\MenuResource;
use Modules\Catalog\Interfaces\V1\MenuRepositoryInterface;

class MenuRepository implements MenuRepositoryInterface
{
    public function index($request)
    {
        return MenuResource::collection(Menu::with(['categories'])->get());
    }

    public function store($request)
    {
        $menu = Menu::create($request->validated());
        if($request->filled('categories_ids'))
        $menu->categories()->attach($request->categories_ids);
        return new MenuResource($menu);
    }

    public function show($request, $menu)
    {
        $menu->load(['categories']);
        return new MenuResource($menu);
    }
}
