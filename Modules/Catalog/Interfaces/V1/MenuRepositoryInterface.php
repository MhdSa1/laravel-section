<?php

namespace Modules\Catalog\Interfaces\V1;

use Modules\Catalog\Entities\Menu;

interface MenuRepositoryInterface
{
    public function index($request);
    public function store($request);
    public function show($request, Menu $menu);
}
