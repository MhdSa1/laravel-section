<?php

namespace Modules\Catalog\Interfaces\V1;

use Modules\Catalog\Entities\Item;

interface ItemRepositoryInterface
{
    public function index($request);
    public function store($request);
    public function show($request, Item $item);
}
