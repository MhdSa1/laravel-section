<?php

namespace Modules\Catalog\Interfaces\V1;

use Modules\Catalog\Entities\Category;

interface CategoryRepositoryInterface
{
    public function index($request);
    public function store($request);
    public function show($request, Category $category);
}
