<?php

namespace Modules\Catalog\Actions;


use Modules\Catalog\Entities\Category;


class CategoryStoreAction
{


    public static function execute($request)
    {
        $check_mixed = Category::where('id', $request->parent_id)->first();
        if ($check_mixed?->parent_id != null) {
            abort(400, 'sub category cant be mixed');
        }

        if ($request->parent_id) {
            $children_count = Category::where('parent_id', $request->parent_id)->count();
            if ($children_count >= 4) {
                abort(400, 'you cant add more than 4 children');
            }
        }
        return  Category::create($request->validated());
    }
}
