<?php

namespace Modules\Catalog\Actions;

use Modules\Catalog\Entities\Item;

class ItemStoreAction
{
    public static function execute($request)
    {
       return Item::create($request->validated());
    }
}
