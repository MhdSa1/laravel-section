<?php

namespace Modules\Catalog\Policies;

use Modules\Auth\Entities\Role;
use Modules\Auth\Entities\User;
use Modules\Auth\Enums\RoleEnum;

class ItemPolicy
{
    public static function index()
    {
        return true;
    }

    public static function store(User $user)
    {
        $admin_role_id = Role::where("name", RoleEnum::admin->value)->pluck("id")->first();
        if ($user->role_id !== $admin_role_id) {
            return false;
        }
        return true;
    }

    public static function show()
    {
        return true;
    }

    public static function update()
    {
        return true;
    }

    public static function delete()
    {
        return true;
    }
}
