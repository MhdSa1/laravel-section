<?php

namespace Modules\Catalog\Http\Resources\Menus;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Catalog\Http\Resources\Categories\CategoryResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'categories' =>  CategoryResource::collection($this->categories),

        ];
    }
}
