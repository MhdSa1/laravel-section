<?php

namespace Modules\Catalog\Http\Resources\Categories;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Catalog\Http\Resources\Items\ItemShowResource;

class CategoryShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'items' => ItemShowResource::collection($this->items),

        ];
    }
}
