<?php

namespace Modules\Catalog\Http\Resources\Categories;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Catalog\Http\Resources\Items\ItemShowResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'items' => ItemShowResource::collection($this->items),
            'parent' => $this->when(!$this->is_parent, new CategoryShowResource($this->parent)),
            'children' => $this->when($this->is_parent, CategoryShowResource::collection($this->children)),
        ];
    }
}
