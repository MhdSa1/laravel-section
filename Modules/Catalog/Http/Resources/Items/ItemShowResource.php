<?php

namespace Modules\Catalog\Http\Resources\Items;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Catalog\Http\Resources\Categories\CategoryResource;

class ItemShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
