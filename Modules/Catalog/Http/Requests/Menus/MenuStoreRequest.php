<?php

namespace Modules\Catalog\Http\Requests\Menus;

use App\Http\Requests\BaseRequest;
use Modules\Catalog\Policies\MenuPolicy;

class MenuStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:40'],
            'categories_ids' => ['sometimes', 'array'],
            'categories_ids.*' => ['exists:categories,id'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return MenuPolicy::store(auth()->user());
    }
}
