<?php

namespace Modules\Catalog\Http\Requests\Menus;

use App\Http\Requests\BaseRequest;
use Modules\Catalog\Policies\MenuPolicy;

class MenuIndexRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return MenuPolicy::index();
    }
}
