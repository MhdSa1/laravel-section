<?php

namespace Modules\Catalog\Http\Requests\Categories;

use App\Http\Requests\BaseRequest;
use Modules\Catalog\Policies\CategoryPolicy;

class CategoryStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:40'],
            'parent_id' => ['sometimes', 'exists:categories,id']
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return CategoryPolicy::store(auth()->user());
    }
}
