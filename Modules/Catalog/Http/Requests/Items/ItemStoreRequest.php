<?php

namespace Modules\Catalog\Http\Requests\Items;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;
use Modules\Catalog\Policies\ItemPolicy;

class ItemStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:40', Rule::unique('items')->where(function ($query) {
                return $query->where('category_id', $this->input('category_id'));
            }),],
            'category_id' => ['sometimes', 'exists:categories,id']
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ItemPolicy::store(auth()->user());
    }
}
