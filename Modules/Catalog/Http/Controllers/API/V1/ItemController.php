<?php

namespace Modules\Catalog\Http\Controllers\API\V1;

use App\Http\Controllers\BaseController;
use Modules\Catalog\Entities\Item;
use Modules\Catalog\Http\Requests\Items\ItemIndexRequest;
use Modules\Catalog\Http\Requests\Items\ItemShowRequest;
use Modules\Catalog\Http\Requests\Items\ItemStoreRequest;
use Modules\Catalog\Interfaces\V1\ItemRepositoryInterface;

class ItemController extends BaseController
{ 
    public function __construct(private ItemRepositoryInterface $itemRepositoryInterface)
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * @param ItemIndexRequest $request
     */
    public function index(ItemIndexRequest $request)
    {
        return apiResponse(
            'retrieve all items',
            $this->itemRepositoryInterface->index($request),
            200
        );
    }

    /**
     * Show the form for creating a new resource.
     * @param ItemStoreRequest $request
     */
    public function store(ItemStoreRequest $request)
    {
        return apiResponse(
            'create new item',
            $this->itemRepositoryInterface->store($request),
            201
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param ItemShowRequest $request
     * @param Item $item
     */
    public function show(ItemShowRequest $request, Item $item)
    {
        return apiResponse(
            'retrieve item',
            $this->itemRepositoryInterface->show($request, $item),
            200
        );
    }
}
