<?php

namespace Modules\Catalog\Http\Controllers\API\V1;

use App\Http\Controllers\BaseController;
use Modules\Catalog\Entities\Category;
use Modules\Catalog\Interfaces\V1\CategoryRepositoryInterface;
use Modules\Catalog\Http\Requests\Categories\CategoryIndexRequest;
use Modules\Catalog\Http\Requests\Categories\CategoryShowRequest;
use Modules\Catalog\Http\Requests\Categories\CategoryStoreRequest;

class CategoryController extends BaseController
{

    public function __construct(private CategoryRepositoryInterface $categoryRepository)
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * @param CategoryIndexRequest $request
     */
    public function index(CategoryIndexRequest $request)
    {
        return apiResponse(
            'retrieve all categories',
            $this->categoryRepository->index($request),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param CategoryStoreRequest $request
     */
    public function store(CategoryStoreRequest $request)
    {
        return apiResponse(
            'store new category',
            $this->categoryRepository->store($request),
            201
        );
    }

    /**
     * Show the specified resource.
     * @param CategoryShowRequest $request
     * @param Category $category
     */
    public function show(CategoryShowRequest $request, Category $category)
    {
        return apiResponse(
            'show category',
            $this->categoryRepository->show($request, $category),
            200
        );
    }
}
