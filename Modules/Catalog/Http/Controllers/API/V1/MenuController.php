<?php

namespace Modules\Catalog\Http\Controllers\API\V1;

use App\Http\Controllers\BaseController;
use Modules\Catalog\Entities\Menu;
use Modules\Catalog\Http\Requests\Menus\MenuIndexRequest;
use Modules\Catalog\Http\Requests\Menus\MenuShowRequest;
use Modules\Catalog\Http\Requests\Menus\MenuStoreRequest;
use Modules\Catalog\Interfaces\V1\MenuRepositoryInterface;

class MenuController extends BaseController
{

    public function __construct(private MenuRepositoryInterface $menuRepositoryInterface)
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * @param MenuIndexRequest $request
     */
    public function index(MenuIndexRequest $request)
    {
        return apiResponse(
            'retrieve all menus',
            $this->menuRepositoryInterface->index($request),
            200
        );
    }


    /**
     * Store a newly created resource in storage.
     * @param MenuStoreRequest $request
     */
    public function store(MenuStoreRequest $request)
    {
        return apiResponse(
            'store new menu',
            $this->menuRepositoryInterface->store($request),
            201
        );
    }

    /**
     * Show the specified resource.
     * @param MenuShowRequest $request
     * @param Menu $menu
     */
    public function show(MenuShowRequest $request, Menu $menu)
    {
        return apiResponse(
            'show menu',
            $this->menuRepositoryInterface->show($request, $menu),
            200
        );
    }
}
