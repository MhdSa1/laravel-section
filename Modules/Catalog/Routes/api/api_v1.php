<?php

use Illuminate\Support\Facades\Route;
use Modules\Catalog\Http\Controllers\API\V1\CategoryController;
use Modules\Catalog\Http\Controllers\API\V1\ItemController;
use Modules\Catalog\Http\Controllers\API\V1\MenuController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('categories', [CategoryController::class, 'index']);
Route::get('categories/{category}', [CategoryController::class, 'show']);
Route::post('categories', [CategoryController::class, 'store'])->middleware('auth:sanctum');

Route::get('items', [ItemController::class, 'index']);
Route::get('items/{item}', [ItemController::class, 'show']);
Route::post('items', [ItemController::class, 'store'])->middleware('auth:sanctum');


Route::get('menus', [MenuController::class, 'index']);
Route::get('menus/{menu}', [MenuController::class, 'show']);
Route::post('menus', [MenuController::class, 'store'])->middleware('auth:sanctum');
