<?php

namespace Modules\Catalog\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Item extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'category_id',
        'price'
    ];


    //SECTION - Relations
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
