<?php

namespace Modules\Catalog\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MenuCategory extends Model
{
    use HasFactory;

    protected $fillable = ['menu_id', 'category_id'];

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
    public function items()
    {
        return $this->hasMany(Category::class);
    }
}
