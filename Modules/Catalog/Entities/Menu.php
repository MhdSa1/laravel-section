<?php

namespace Modules\Catalog\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Menu extends Model
{
    use HasFactory;
    protected $fillable = ['name'];

    //SECTION - Relations
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'menu_categories');
    }
}
