<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AdminInformation extends Model
{
    use HasFactory;

    protected $table= "admin_information";
    protected $fillable = [
        'info',
        'user_id',
    ];

    public function user(){
        return $this->belongsTo(User::class,);
    }
}
