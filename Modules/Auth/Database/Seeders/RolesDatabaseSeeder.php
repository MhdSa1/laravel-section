<?php

namespace Modules\Auth\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Auth\Entities\Role;
use Modules\Auth\Enums\RoleEnum;


class RolesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $admin_role = Role::where('name', RoleEnum::admin->value)->first();
        $user_role = Role::where('name', RoleEnum::user->value)->first();


        if ($admin_role == null) {
            $admin_role = Role::create([
                'name' => RoleEnum::admin->value,
            ]);
        } else {
            $admin_role->update([
                'name' => RoleEnum::admin->value,
            ]);
        }

        if ($user_role == null) {
            $user_role = Role::create([
                'name' => RoleEnum::user->value,
            ]);
        } else {
            $user_role->update([
                'name' => RoleEnum::user->value,
            ]);
        }
    }
}
