<?php

namespace Modules\Auth\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Auth\Entities\Role;
use Modules\Auth\Entities\User;
use Modules\Auth\Enums\RoleEnum;
class UsersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $admin_role = Role::where('name', RoleEnum::admin->value)->first();
        $user_role = Role::where('name',RoleEnum::user->value)->first();


        $admin = User::where('email', 'admin@email.com')->first();

        if ($admin == null) {
            $admin = User::create([
                'name' => 'Admin',
                'role_id' => $admin_role->id,
                'email' => 'admin@email.com',
                'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',

            ]);
        } else {
            $admin->update([
                'name' => 'Admin',
                'role_id' => $admin_role->id,
                'email' => 'admin@miroworld.com',
                'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
            ]);
        }


        $user = User::where('email', 'user@email.com')->first();

        if ($user == null) {
            $user = User::create([
                'name' => 'user',
                'role_id' => $user_role->id,
                'email' => 'user@email.com',
                'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',

            ]);
        } else {
            $user->update([
                'name' => 'user',
                'role_id' => $user_role->id,
                'email' => 'user@email.com',
                'password' => '$2a$12$NtaOVNCsx1Lt06ozIMYj2uaaB.Jqjy8pS3JsFT/b6NMXbptN2e6km',
            ]);
        }
    }
}
