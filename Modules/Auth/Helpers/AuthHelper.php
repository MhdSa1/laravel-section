<?php

namespace Modules\Auth\Helpers;

class AuthHelper
{

    public static function createToken($user): string
    {
        return  $user->createToken('token')->plainTextToken;
    }

    public static function userLogoutAllDevices($user)
    {
        $user->tokens()->delete();
    }
}
