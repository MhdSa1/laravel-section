<?php

namespace Modules\Auth\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;


class AdminInformationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'info' => $this->info,

        ];
    }
}
