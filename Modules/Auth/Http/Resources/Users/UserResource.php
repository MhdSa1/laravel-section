<?php

namespace Modules\Auth\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Auth\Enums\RoleEnum;
use Modules\Auth\Http\Resources\RoleResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'name' => $this->name,
            'role' => new RoleResource($this->role),
            'admin_info'=>$this->when(($this->role->name==RoleEnum::admin->value), new AdminInformationResource($this->adminInformation))
        ];
    }
}
