<?php

namespace Modules\Auth\Http\Controllers\API\V1;

use App\Http\Controllers\BaseController;
use Modules\Auth\Helpers\AuthHelper;
use Modules\Auth\Http\Requests\Auth\LoginRequest;
use Modules\Auth\Http\Requests\Auth\RegisterRequest;
use Modules\Auth\Http\Resources\Users\UserResource;
use Modules\Auth\Interfaces\V1\AuthRepositoryInterface;

class AuthController extends BaseController
{
    private $authRepository;
    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
        parent::__construct();
    }


    /**
     * Register in the system with the ID card and phone number.
     * @param RegisterRequest $requestW
     * @return 
     */
    public function register(RegisterRequest $request)
    {
        $user = $this->authRepository->register($request);

        return apiResponse(
            "register new admin",
            [
                'user' => new UserResource($user),
                "token" => AuthHelper::createToken($user),
            ],
            201,
        );
    }


    /**
     * Login in the system with phone number and password.
     * @param LoginRequest $request
     * @return 
     */
    public function login(LoginRequest $request)
    {
        $auth_user = $this->authRepository->login($request);
        return apiResponse(
            'user login successfully',
            [
                "user" => $auth_user,
                "token" => AuthHelper::createToken($auth_user),
            ],
            200
        );
    }


    /**
     * Logout and delete the current profile access token
     * @return 
     */
    public function logout()
    {
        return apiResponse(
            'user logout successfully',
            $this->authRepository->logout(),
            200
        );
    }
}
