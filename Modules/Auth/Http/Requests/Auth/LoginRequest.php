<?php

namespace Modules\Auth\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Entities\User;
use Modules\Auth\Policies\AuthPolicy;

class LoginRequest extends BaseRequest
{

    public function rules()
    {
        return [
            'email' => ['required','exists:users,email'],
            'password' => ['required'],
            'user' => ['prohibited'],
        ];
    }


    public function passedValidation()
    {
        $user = User::where('email', $this->email)->first();

        if (!$user || !(Hash::check($this->password, $user->password))) {
            abort(401,"Wrong credentials");
        }
        $this->merge(['user' => $user]);
        return true;
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AuthPolicy::login();
    }
}
