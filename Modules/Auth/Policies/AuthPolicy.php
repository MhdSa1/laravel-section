<?php

namespace Modules\Auth\Policies;

use Modules\Profile\Entities\Profile;

class AuthPolicy
{

    public static function login()
    {
        return true;
    }

    public static function register()
    {
        return true;
    }

    public static function logout()
    {
        return true;
    }
}
