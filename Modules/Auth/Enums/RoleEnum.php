<?php

namespace Modules\Auth\Enums;

enum RoleEnum:string {
    case admin = 'admin';
    case user = 'user';
}