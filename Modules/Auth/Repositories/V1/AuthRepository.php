<?php

namespace Modules\Auth\Repositories\V1;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Entities\Role;
use Modules\Auth\Entities\User;
use Modules\Auth\Enums\RoleEnum;
use Modules\Auth\Http\Resources\Users\UserResource;
use Modules\Auth\Interfaces\V1\AuthRepositoryInterface;

class AuthRepository  implements AuthRepositoryInterface
{
    public function register($request)
    {
        $data = $request->validated();
        $data['password'] = Hash::make($data['password']);
        $data['role_id'] = Role::where('name', RoleEnum::admin->value)->first()->id;
        $user = User::create($data);
        if ($request->info) {
            $user->adminInformation()->create(['info' => $request->info]);
        }
        return $user;
    }

    public function login($request)
    {
        $user = $request->user; // the user merged in form request after validation

        return new UserResource($user);
    }

    public function logout()
    {
        Auth::user()->currentAccessToken()?->delete();
    }
}
