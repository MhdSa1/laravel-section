<?php

namespace Modules\Auth\Interfaces\V1;

interface AuthRepositoryInterface
{
    public function register($request);
    public function login($request);
    public function logout();
}
