<?php

/**
 * Creates an API response object.
 *
 * @param string $message The message to include in the response.
 * @param mixed $data The data to include in the response, defaults to null.
 * @param int $status The HTTP status code for the response, defaults to 200.
 * @param array|null $meta Any other additional data to include in the response, defaults to null.
 *
 * @return 
 */
function apiResponse($message, $data = [], $status = 200, $meta = null, $extra = null)
{
    return response()->json(
        [
            'status' => $status,
            'message' => $message,
            'data' => $data,
            'meta' => $meta,
            'extra' => $extra,
        ],
        $status
    );
}

/**
 * Creates an API Error response object.
 *
 * @param string $message The message to include in the response.
 * @param mixed $errors The data to include in the response, defaults to null.
 * @param int $status The HTTP status code for the response

 * @param array|null $other Any other additional data to include in the response, defaults to null.
 *
 * @return 
 */
function apiErrorResponse($message, $status, $errors = [])
{
    return response()->json(
        [
            'status' => $status,
            'message' => $message,
            'errors' => $errors,
        ],
        $status
    );
}


function transferDataWithPagination($data)
{
    return  [
        'items' => $data->items(),
        'pagination' => [
            'total' => $data->total(),
            'perPage' => $data->perPage(),
            'currentPage' => $data->currentPage(),
            'from' => $data->firstItem(),
            'to' => $data->lastItem(),
            'lastPage' => $data->lastPage(),
        ]
    ];
}

function transferData($data)
{
    return  [
        'items' => $data

    ];
}

/**
 * Replaces all spaces in a string with underscores.
 *
 * @param string $str The string to be processed.
 *
 * @return string The processed string with all spaces replaced with underscores.
 */
function replaceSpacesWithUnderscores($str)
{
    $str = str_replace(' ', '_', $str);
    return $str;
}

function removeSpaces(string $string): string
{
    return preg_replace('/\s+/', ' ', trim($string));
}
