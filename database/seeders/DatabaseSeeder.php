<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Auth\Database\Seeders\RolesDatabaseSeeder;
use Modules\Auth\Database\Seeders\UsersDatabaseSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (config('app.env')  == 'local' || config('app.env')  == 'development') {
            $this->call([TruncateAllSeeder::class,]);
        }
        $this->call(
            [
                RolesDatabaseSeeder::class,
                UsersDatabaseSeeder::class,
            ]
        );
    }
}
